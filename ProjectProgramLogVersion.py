import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import math
from math import exp

#DATA
timeData = [0, 0.463014, 0.926027, 1.38904, 1.85205, 2.31507, 2.73699, 3.728767, 4.342466,  6.460274, 6.547945]
logVolData = [math.log(x) for x in [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]]
timeRange = 7
t = np.linspace(0,timeRange)
#plt.figure(figsize=(20, 12))
plt.xlabel('Time (years)', fontsize=15)
plt.ylabel('log Volume', fontsize=15)
plt.plot(timeData, logVolData, 'ro', linestyle='None')

#Exponential Model
expoParam = 1.5162879854524727
expoV = []
for x in t:
    expoV.append(math.log(0.0001*math.exp(expoParam*x))) 
plt.plot(t, expoV, label="Exponential Model")

#Gompertz Model - No Limit
aGomParam = 1.4956297820876445 
bGomParam = -0.004180436966871044
gomV = []
for x in t:
    gomV.append(math.log(0.0001*math.exp((aGomParam/bGomParam)*(1-math.exp(-bGomParam*x)))))   
plt.plot(t, gomV, label="Gompertz Model - No Limit", color='orange')    

#Gompertz Model - Limit - d=2.24
c2LGomParam = 0.23718
d2LGomParam = 2.24
f2LGomParam = 76043
gom2LV = []
for x in t:
    gom2LV.append(math.log(d2LGomParam*exp(-math.log(c2LGomParam)*exp(-f2LGomParam*x))))   
#plt.plot(t, gom2LV, label="Gompertz Model - limit = 2.24", color='green') 
    
#Gompertz Model - Limit - d=4.77
c4LGomParam = 0.21130
d4LGomParam = 4.77
f4LGomParam = 135721
gom4LV = []
for x in t:
    gom4LV.append(math.log(d4LGomParam*exp(-math.log(f4LGomParam)*exp(-c4LGomParam*x))))   
#plt.plot(t, gom4LV, label="Gompertz Model - limit = 4.77", color='crimson')

#Gompertz Model - Limit - d=6.54
c6LGomParam = 0.20210
d6LGomParam = 6.54
f6LGomParam = 175089
gom6LV = []
for x in t:
    gom6LV.append(math.log(d6LGomParam*exp(-math.log(f6LGomParam)*exp(-c6LGomParam*x))))
#plt.plot(t, gom6LV, label="Gompertz Model - limit = 6.54", color='pink') 

#Gompertz Model - Limit - d=11.3
c11LGomParam = 0.17803
d11LGomParam = 11.3
f11LGomParam = 177322
gom11LV = []
for x in t:
    gom11LV.append(math.log(d11LGomParam*exp(-math.log(f11LGomParam)*exp(-c11LGomParam*x))))   
#plt.plot(t, gom11LV, label="Gompertz Model - limit = 11.3", color='limegreen')

#Gompertz Model - Limit - d=38.2
c38LGomParam = 0.29473
d38LGomParam = 38.2
f38LGomParam = 2167814198
gom38LV = []
for x in t:
    gom38LV.append(math.log(d38LGomParam*exp(-math.log(f38LGomParam)*exp(-c38LGomParam*x))))
plt.plot(t, gom38LV, label="Gompertz Model - limit = 38.2", color='blueviolet')

#Gompertz Model - 3 Vars Determined
cLGomParam = 0.36804
dLGomParam = 16.3158
fLGomParam = 102090665414
gomLV = []
for x in t:
    gomLV.append(math.log(dLGomParam*exp(-math.log(fLGomParam)*exp(-cLGomParam*x)))) 
plt.plot(t, gomLV, label="Gompertz Model - 3 Vars Determined", color='violet')

#Bertalanffy Model
def bertDiff(V,t):
    gBertParam = -4.5368806837905206e-13
    hBertParam = -1.4970781437651128 
    dVdt = gBertParam*V**(2/3) - hBertParam*V
    return dVdt
V0 = 0.0001
bertV = odeint(bertDiff,V0,t)
logBertV = [math.log(x) for x in bertV]
plt.plot(t, logBertV, label="Bertalanffy Model", color='gold')


plt.legend()
plt.show()
