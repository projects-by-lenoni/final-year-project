Python files in this repository support the claims made in the final report.

The purpose of each file is detailed below:

ExpoParameters.py - Calculates possible parameters for the exponential model and their accompanying RSS value.
ExpoParameters-Log.py - Calculates the RSS value using the log of the volumes.

GompertzParamsNoLimit.py - Calculates possible parameters for the Gompertz model without a limit and their accompanying RSS value.
GompertzParamsNoLimit-Log.py - Calculates the RSS value using the log of the volumes.

GompertzParamsLimiEsti.py - Calculates possible parameters for the Gompertz model with an estimated limit and their accompanying RSS value.
GompertzParamsLimitEsti-Log.py - Calculates the RSS value using the log of the volumes.

GompertzParams3Variables.py - Calculates possible parameters for the Gompertz model with a limit value and their accompanying RSS value.
GompertzParams3Variables-Log.py - Calculates the RSS value using the log of the volumes.

BertParams3Variables.py - Calculates possible parameters for the Bertalanffy model and their accompanying RSS value.
BertParams3Variables-Log.py - Calculates the RSS value using the log of the volumes RSS value.

ProjectProgram.py - Plots all of the current tumour growth models.
ProjectProgramLogVersion.py - Plots the log of the volumes of the current tumour growth models.

NoTreatment.py - Creates the plot of the developed model of tumour growth with no treatment.

Surgery.py - Creates the plot of tumour growth with surgery using a simple two phase model.
SurgeryRevised.py - Creates the plot of tumour growth with surgery using a volume threshold switch. 

Chemo.py - Creates the plot of the tumour growth with chemotherapy using a periodic function.
ChemoRevised.py - Creates the plot of tumour growth with chemotherapy using a time interval approach.