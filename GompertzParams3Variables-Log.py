from tabulate import tabulate
import numpy as np
import matplotlib.pyplot as plt
import math
from math import exp
from scipy.optimize import fsolve
import statistics

#Data used to determine parameters
timeData = [0, 0.4630, 0.9260, 1.389, 1.852, 2.315, 2.736, 3.7287, 4.3424,  6.4602, 6.547945]
volumeData = [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]
logVolData = [math.log(x) for x in [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]]

#GOMPERTZ PARAMS - 3 Simultaneous Equations
def getGompParams(data1, data2, data3): #Calculates parameters c, d and f when given three data points
    def equations(vars):
        c, d, f = vars
        eq1 = math.log(data1[1]/abs(d)) + math.log(abs(f))*exp(-c*data1[0])
        eq2 = math.log(data2[1]/abs(d)) + math.log(abs(f))*exp(-c*data2[0])
        eq3 = math.log(data3[1]/abs(d)) + math.log(abs(f))*exp(-c*data3[0])
        return [eq1, eq2, eq3]
    
    c, d, f = fsolve(equations, (0.4, 11.3, 170000))
    return c, d, f

def neighDataPoints(): #Chooses the neighbouring data points
    params = []
    for x in range(0, len(timeData)-2):
        data1 = [timeData[x], volumeData[x]]
        data2 = [timeData[x+1], volumeData[x+1]]
        data3 = [timeData[x+2], volumeData[x+2]]
        params.append(getGompParams(data1, data2, data3))
    return params

def averages():  
    paramC = []
    paramD = []
    paramF = []
    for x in neighDataPoints():
        paramC.append(x[0])
        paramD.append(x[1])
        paramF.append(x[2])
    meanC = sum(paramC)/len(paramC)
    meanD = sum(paramD)/len(paramD)
    meanF = sum(paramF)/len(paramF)
    medianC = statistics.median(paramC)
    medianD = statistics.median(paramD)
    medianF = statistics.median(paramF)
    return meanC, meanD, meanF, medianC, medianD, medianF



def gompVolLogEstimates(paramC, paramD, paramF): #Works out the estimated volumes using the given parameter
    logVols = []
    for x in timeData:
        logVols.append(math.log(abs(paramD*exp(-math.log(abs(paramF))*exp(-paramC*x)))))
    return(logVols)
    
def residualsSquared(estiVols): #Calculates the squared difference between the estimated log vols and measured vols
    resiArray = []
    for x in range(0,len(logVolData)):
        resiArray.append((estiVols[x]-logVolData[x])**2)
    return resiArray    


print("GOMPERTZ PARAMETERS DETERMINING 3 VARIABLES")

cParams = []
dParams = []
fParams = []
for i in range(0, len(neighDataPoints())):
    cParams.append(neighDataPoints()[i][0])
    dParams.append(neighDataPoints()[i][1])
    fParams.append(neighDataPoints()[i][2])
timesUsed = ["t0t1t2", "t1t2t3", "t2t3t4", "t3t4t5", "t4t5t6", "t5t6t7", "t6t7t8", "t7t8t9", "t8t9t10"]
print(tabulate({"Times Used": timesUsed, "c": cParams, "d": dParams, "f": fParams}, headers="keys"),"\n")

print("Using neighbouring data points to determine the parameters c, d and f")
for i in range(0,9):
    paramC = cParams[i]
    paramD = dParams[i]
    paramF = fParams[i]
    print("   c = ", paramC,"\n  d  = ", paramD, "\n   f = ", paramF)
    print(tabulate({"Time": timeData,"Estimated Vol.": gompVolLogEstimates(paramC,paramD,paramF), "Residual Squared": residualsSquared(gompVolLogEstimates(paramC,paramD,paramF))}, headers="keys"),"\n")
    print("Residual Sum of Squares = ", sum( residualsSquared(gompVolLogEstimates(paramC,paramD,paramF))))
    print("Residual Sum of Squares (No Extrapolation) = ", sum( residualsSquared(gompVolLogEstimates(paramC,paramD,paramF))[5:len(logVolData)]))
    print("\n---------------------------------------------------")


print("Using the arithmetic mean of neighbouring data points to determine parameters c, d and f")
print("   c = ", averages()[0], "\n    d = ",averages()[1], "\n   f = ", averages()[2])
print(tabulate({"Time": timeData,"Estimated Vol.": gompVolLogEstimates(averages()[0],averages()[1],averages()[2]), "Residual Squared": residualsSquared(gompVolLogEstimates(averages()[0],averages()[1],averages()[2]))}, headers="keys"),"\n")
print("Residual sum = ", sum( residualsSquared(gompVolLogEstimates(averages()[0],averages()[1],averages()[2]))))
print("Residual Sum of Squares (No Extrapolation) = ", sum( residualsSquared(gompVolLogEstimates(averages()[0],averages()[1],averages()[2]))[5:len(logVolData)]))
print("\n---------------------------------------------------")
