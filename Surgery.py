import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import math

#DATA
timeData = [0, 0.463014, 0.926027, 1.38904, 1.85205, 2.31507, 2.73699, 3.728767, 4.342466,  6.460274, 6.547945]
volumeData = [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]
timeRange = 7
t = np.linspace(0,timeRange)
#plt.figure(figsize=(20, 12))
plt.xlabel('Time (years)', fontsize=15)
plt.ylabel(r'$Volume (x10^{-4} mm^3)$', fontsize=15)
plt.plot(timeData, volumeData, 'ro', linestyle='None')

#Surgery - Two Phase
def surgeryBefore(V, t): #Exponential model for phase 1
    expoParam = 1.5162879854524727
    dVdt = expoParam*V
    return dVdt
def surgeryAfter(V, t): #Gompertz model for phase 2
    cLGomParam = 0.29473011238198343
    dLGomParam = 38.2
    dVdt = cLGomParam*V*math.log(dLGomParam/V)
    return dVdt

V0 = 0.0001 #Initial tumour volume
surgeryTime = 6.5 #Time of surgery
percentRemoved = 90 #Percent of tumour removed 
if 100%timeRange == 0:
    numPointsBefore = (surgeryTime/timeRange)*100
    numPointsAfter = 100 - numPointsBefore
elif 100%timeRange != 0:
    numPointsBefore = round((surgeryTime/timeRange)*100)
    numPointsAfter = 100 - numPointsBefore

tArrBefore = np.linspace(0, surgeryTime, int(numPointsBefore))
tArrAfter = np.linspace(surgeryTime, timeRange, int(numPointsAfter))
surgeryBefore = odeint(surgeryBefore, V0, tArrBefore)
surgeryAfter = odeint(surgeryAfter, surgeryBefore[-1] - surgeryBefore[-1]*percentRemoved/100, tArrAfter)

#Calculated volume arrays are joined together and plotted
surgery = []
time = []
for i in range(0, len(surgeryBefore)):
    surgery.append(surgeryBefore[i])
    time.append(tArrBefore[i])
for j in range(0, len(surgeryAfter)):
    surgery.append(surgeryAfter[j])
    time.append(tArrAfter[j])
plt.plot(time, np.concatenate(surgery), label="New Model - Surgery", color='goldenrod')
    

plt.legend()
plt.show()