from tabulate import tabulate
import math
from math import exp
from scipy.optimize import fsolve
import statistics

#Data used to determine parameters
timeData = [0, 0.4630, 0.9260, 1.389, 1.852, 2.315, 2.736, 3.7287, 4.3424,  6.4602, 6.547945]
volumeData = [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]

#GOMPERTZ PARAMS - With an estimated limit
d = 38.2 #Limit value d should be changed for different calculations
def getGompParams(data1, data2): #Calculates parameters c and f when given two data points
    def equations(vars):
        c, f = vars
        eq1 = math.log(data1[1]/d) + math.log(abs(f))*exp(-c*data1[0])
        eq2 = math.log(data2[1]/d) + math.log(abs(f))*exp(-c*data2[0])
        return [eq1, eq2]
    
    c, f = fsolve(equations, (0.1, 10))
    return c, f

def neighDataPoints(): #Chooses the neighbouring data points
    params = []
    for x in range(1, len(timeData)-2):
        data1 = [timeData[x], volumeData[x]]
        data2 = [timeData[x+1], volumeData[x+1]]
        params.append(getGompParams(data1, data2))
    return params

def endPoints(): #Calculates the parameters when using the end pieces of data
    data1 = [timeData[1], volumeData[1]]
    data2 = [timeData[10], volumeData[10]]
    return getGompParams(data1, data2)

def averages():  
    paramC = []
    paramF = []
    for x in neighDataPoints():
        paramC.append(x[0])
        paramF.append(x[1])
    meanC = sum(paramC)/len(paramC)
    meanF = sum(paramF)/len(paramF)
    medianC = statistics.median(paramC)
    medianF = statistics.median(paramF)
    return meanC, meanF, medianC, medianF



def gompVolEstimates(paramC, paramF): #Works out the estimated volumes using the given parameters
    vols = []
    for x in timeData:
        vols.append(d*exp(-math.log(paramF)*exp(-paramC*x)))
    return(vols)
    
def residualsSquared(estiVols): #Calculates the squared difference between the estimated vols and measured vols
    resiArray = []
    for x in range(0,len(volumeData)):
        resiArray.append((estiVols[x]-volumeData[x])**2)
    return resiArray 


cParams = []
fParams = []
for i in range(0, len(neighDataPoints())):
    cParams.append(neighDataPoints()[i][0])
    fParams.append(neighDataPoints()[i][1])
timesUsed = ["t1t2", "t2t3", "t3t4", "t4t5", "t5t6", "t6t7", "t7t8", "t8t9"]
print(tabulate({"Times Used": timesUsed, "c": cParams, "f": fParams}, headers="keys"),"\n")

print("Using neighbouring data points to determine the parameters a and c")
for i in range(0,8):
    paramC = cParams[i]
    paramF = fParams[i]
    print("   c = ", paramC, "\n   f = ", paramF)
    print(tabulate({"Time": timeData,"Estimated Vol.": gompVolEstimates(paramC,paramF), "Residual Squared": residualsSquared(gompVolEstimates(paramC,paramF))}, headers="keys"),"\n")
    print("Residual Sum of Squares = ", sum( residualsSquared(gompVolEstimates(paramC,paramF))))
    print("\n---------------------------------------------------")

print("Using end data points to determine parameters c and f")
print("   c = ", endPoints()[0], "\n   f = ", endPoints()[1])
print(tabulate({"Time": timeData,"Estimated Vol.": gompVolEstimates(endPoints()[0],endPoints()[1]), "Residual Squared": residualsSquared(gompVolEstimates(endPoints()[0],endPoints()[1]))}, headers="keys"),"\n")
print("Residual Sum of Squares = ", sum( residualsSquared(gompVolEstimates(endPoints()[0],endPoints()[1]))))
print("\n---------------------------------------------------")

print("Using the arithmetic mean of neighbouring data points to determine parameters c and f")
print("   c = ", averages()[0], "\n   f = ", averages()[1])
print(tabulate({"Time": timeData,"Estimated Vol.": gompVolEstimates(averages()[0],averages()[1]), "Residual Squared": residualsSquared(gompVolEstimates(averages()[0],averages()[1]))}, headers="keys"),"\n")
print("Residual Sum of Squares = ", sum( residualsSquared(gompVolEstimates(averages()[0],averages()[1]))))
print("\n---------------------------------------------------")

print("Using the median of neighbouring data points to determine parameters c and f")
print("   c = ", averages()[2], "\n   f = ", averages()[3])
print(tabulate({"Time": timeData,"Estimated Vol.": gompVolEstimates(averages()[2],averages()[3]), "Residual Squared": residualsSquared(gompVolEstimates(averages()[2],averages()[3]))}, headers="keys"),"\n")
print("Residual Sum of Squares = ", sum( residualsSquared(gompVolEstimates(averages()[2],averages()[3]))))
print("\n---------------------------------------------------")
