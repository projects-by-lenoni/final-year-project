from tabulate import tabulate
import math
from math import exp
from scipy.optimize import fsolve
import statistics


#Data used to determine parameters
timeData = [0, 0.4630, 0.9260, 1.389, 1.852, 2.315, 2.736, 3.7287, 4.3424,  6.4602, 6.547945]
volumeData = [0.0001, 0.0002, 0.0004, 0.0009, 0.0017, 0.0034, 0.0063, 0.0264, 0.0968, 1.5534, 2.0508]

#GOMPERTZ PARAMS - No Given Limit
def getGompParams(data1, data2): #Calculates parameters a and b when given two data points
    def equations(vars):
        a, b = vars
        eq1 = (a/b)*(1-exp(-b*data1[0])) - math.log(data1[1]/0.0001)
        eq2 = (a/b)*(1-exp(-b*data2[0])) - math.log(data2[1]/0.0001)
        
        return [eq1, eq2]
    a, b = fsolve(equations, (1.1, 0.001))
    return a, b

def neighDataPoints(): #Chooses the neighbouring data points
    params = []
    for x in range(1, len(timeData)-1):
        data1 = [timeData[x], volumeData[x]]
        data2 = [timeData[x+1], volumeData[x+1]]
        params.append(getGompParams(data1, data2))
    return params

def endPoints(): #Calculates the parameters when using the end points of data
    data1 = [timeData[1], volumeData[1]]
    data2 = [timeData[10], volumeData[10]]
    return getGompParams(data1, data2)

def averages(): #Calculates the mean and median values of arrays
    paramA = []
    paramB = []
    for x in neighDataPoints():
        paramA.append(x[0])
        paramB.append(x[1])
    meanA = sum(paramA)/len(paramA)
    meanB = sum(paramB)/len(paramB)
    medianA = statistics.median(paramA)
    medianB = statistics.median(paramB)
    return meanA, meanB, medianA, medianB



def gompVolEstimates(paramA, paramB): #Works out the estimated volumes using the given parameters
    vols = []
    for x in timeData:
        vols.append(0.0001*math.exp((paramA/paramB)*(1-math.exp(-paramB*x))))
    return(vols)
    
def residualsSquared(estiVols): #Calculates thes squared difference between the estimated vols and measured vols
    resiArray = []
    for x in range(0,len(volumeData)):
        resiArray.append((estiVols[x]-volumeData[x])**2)
    return resiArray   


aParams = []
bParams = []
for i in range(0, len(neighDataPoints())):
    aParams.append(neighDataPoints()[i][0])
    bParams.append(neighDataPoints()[i][1])
timesUsed = ["t1t2", "t2t3", "t3t4", "t4t5", "t5t6", "t6t7", "t7t8", "t8t9", "t9t10"]
print(tabulate({"Times Used": timesUsed, "a": aParams, "b": bParams}, headers="keys"),"\n")

print("Using neighbouring data points to determine the parameters a and b")
for i in range(0,9):
    paramA = aParams[i]
    paramB = bParams[i]
    print("   a = ", paramA, "\n   b = ", paramB)
    print(tabulate({"Time": timeData,"Estimated Vol.": gompVolEstimates(paramA,paramB), "Residual Squared": residualsSquared(gompVolEstimates(paramA,paramB))}, headers="keys"),"\n")
    print("Residual Sum of Squares = ", sum( residualsSquared(gompVolEstimates(paramA,paramB))))
    print("\n---------------------------------------------------")

print("Using end data points to determine parameters a and b")
print("   a = ", endPoints()[0], "\n   b = ", endPoints()[1])
print(tabulate({"Time": timeData,"Estimated Vol.": gompVolEstimates(endPoints()[0],endPoints()[1]), "Residual Squared": residualsSquared(gompVolEstimates(endPoints()[0],endPoints()[1]))}, headers="keys"),"\n")
print("Residual Sum of Squares = ", sum( residualsSquared(gompVolEstimates(endPoints()[0],endPoints()[1]))))
print("\n---------------------------------------------------")

print("Using the arithmetic mean of neighbouring data points to determine parameters a and b")
print("   a = ", averages()[0], "\n   b = ", averages()[1])
print(tabulate({"Time": timeData,"Estimated Vol.": gompVolEstimates(averages()[0],averages()[1]), "Residual Squared": residualsSquared(gompVolEstimates(averages()[0],averages()[1]))}, headers="keys"),"\n")
print("Residual Sum of Squares = ", sum( residualsSquared(gompVolEstimates(averages()[0],averages()[1]))))
print("\n---------------------------------------------------")

print("Using the median of neighbouring data points to determine parameters a and b")
print("   a = ", averages()[2], "\n   b = ", averages()[3])
print(tabulate({"Time": timeData,"Estimated Vol.": gompVolEstimates(averages()[2],averages()[3]), "Residual Squared": residualsSquared(gompVolEstimates(averages()[2],averages()[3]))}, headers="keys"),"\n")
print("Residual Sum of Squares = ", sum( residualsSquared(gompVolEstimates(averages()[2],averages()[3]))))
print("\n---------------------------------------------------")

